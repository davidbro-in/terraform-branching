### Dynamic URL from AWS instance
resource "local_file" "publicDNS" {
  content = templatefile("deploy.env.tmpl",
    {
      plublic-dns = aws_eip.one.public_dns
    }
  )
  filename = "../../deploy.env"
}

# Terraform branching

## Description

This project contains a base configuration trying to use git branching strategy
while deploying infrastructure using terraform.

## Installation

This project can be deployed in AWS as well as locally. **An AWS account is 
needed, be aware of AWS cost**.

### Local development

While working locally, there are some options:

1. Deploy in AWS, from local.
1. Deploy locally

In order to work locally, you need to configure your AWS credentials and some
other environment variables.

```sh
export AWS_ACCESS_KEY_ID=<generated in AWS IAM>
export AWS_SECRET_ACCESS_KEY=<generated in AWS IAM>
export CI_COMMIT_REF_SLUG=<branch you are working on>
export TF_STATE_NAME=$CI_COMMIT_REF_SLUG
```

### GitLab CI config

The same variables locally needed, needs to be set in GitLab. Visit 
[CI/CI Settings](../../settings/ci_cd) and set `AWS_ACCESS_KEY_ID` and
`AWS_SECRET_ACCESS_KEY`.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

